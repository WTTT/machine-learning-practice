import cv2
import matplotlib
import numpy

face_cascade = cv2.CascadeClassifier('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/haarcascade_frontalface_default.xml')

img  = [cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi1.jpg'), cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/phongCanh1.jpg')
, cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi2.jpg'), cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi3.jpg'),
cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/phongCanh2.jpg'), cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi4.jpg'), 
cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi5.jpg'), cv2.imread('C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/nguoi6.png'), cv2.imread('C:/Users/ADMIN/Downloads/download.jpg')]

for k in range(len(img)):
    # chuyển ảnh về các pixel đen và trắng
    gray = cv2.cvtColor(img[k], cv2.COLOR_BGR2GRAY)
    # xác định khuôn mặt từ gray ở trên
    faces = face_cascade.detectMultiScale(gray, 1.1, 2)
    
    # vẽ khung chỉ rõ khuôn mặt
    for (x, y, w, h) in faces:
        cv2.rectangle(img[k], (x, y), (x + w, y + h), (255, 0, 0), 2)
    # hiển thị
    cv2.imshow('img', img[k])
    cv2.waitKey()

"""
    Phương pháp dùng là Harr-like feature
    "Xám hóa" các ảnh rồi đưa một hình chữ nhật chạy từ góc trên bên trái của ảnh
    so sánh với các hình chữ nhật trong Harr-like feature để tìm ra khuôn mặt
    Dựa vào scaleFactor, số lân cận và độ phân giải, to nhỏ của ảnh chọn thì độ chính xác của mặt người sẽ khác nhau trong mỗi ảnh
    Nếu chọn K = 0, hình nào càng lồng hình vuông thì độ giống mặt người càng cao
"""