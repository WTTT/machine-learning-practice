from pyvi import ViTokenizer, ViPosTagger
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

class FeatureTransformer(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.tokenizer = ViTokenizer
        self.pos_tagger = ViPosTagger

    def fit(self, *_):
        return self

    def transform(self, X, y=None, **fit_params):
        result = X.apply(lambda text: self.tokenizer.tokenize(text))
        return result

class NaiveBayesModel(object):
    def __init__(self):
        self.clf = self._init_pipeline()

    @staticmethod
    def _init_pipeline():
        pipe_line = Pipeline([
            ("transformer", FeatureTransformer()),#sử dụng pyvi tiến hành word segmentation
            ("vect", CountVectorizer()),#bag-of-words
            ("tfidf", TfidfTransformer()),#tf-idf
            ("clf", MultinomialNB())#model naive bayes
        ])

        return pipe_line


import pandas as pd
import urllib.request
from bs4 import BeautifulSoup
import codecs

english = []
vietnam = []
path = "C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/Tieng_Anh_Tieng_Viet_Train.txt"
with codecs.open(path, 'r', encoding="UTF-8") as file:
    for line in file.readlines():
        line = line.replace("(", "")
        line = line.replace(")", "")
        line = line.replace("?", ".")
        line = line.replace("!", ".")
        line = line.replace(". ", ".")
        line = line[0:(len(line) - 1)]
        line = line.strip(" ")
        store = line.split(".")
        english.append(store[0])
        vietnam.append(store[1])

path1 = "C:/Users/ADMIN/Desktop/Project/GitLab/machine-learning-khainm-2/Tieng_Anh_Tieng_Viet_Test.txt"
count = 0
v_test = []
e_test = []

with codecs.open(path1, 'r', encoding="UTF-8") as f:
    for line in f.readlines():
        line = line.strip()
        line = line.replace("\n", "")
        line = line.replace(" - ", "-")
        if len(line.split()) != 0:
            list_store = line.split("-")
            v_test.append(list_store[1])
            e_test.append(list_store[0])
all_tests = v_test + e_test
class TextClassificationPredict(object):
    def __init__(self):
        self.test = None

    def get_train_data(self):
        # Tạo train data
        train_data = []
        for e in range(len(english)):
            mydict = {"feature" : english[e], "target" : "Tiếng Anh"}
            train_data.append(mydict)
        for v in range(len(vietnam)):
            mydict = {"feature" : vietnam[v], "target" : "Tiếng Việt"}
            train_data.append(mydict)
        train_data.append({"feature": u"Hôm nay trời đẹp không ?", "target": "Tiếng Việt"})
        train_data.append({"feature": u"Hôm nay thời tiết thế nào ?", "target": "Tiếng Việt"})
        train_data.append({"feature": u"Hôm nay mưa không ?", "target": "Tiếng Việt"})
        train_data.append({"feature": u"Hello there", "target": "Tiếng Anh"})
        train_data.append({"feature": u"Hi, how are you?", "target": "Tiếng Anh"})
        train_data.append({"feature": u"My monster cards", "target": "Tiếng Anh"})
        train_data.append({"feature": u"Hi Kimi", "target": "Tiếng Anh"})
        train_data.append({"feature": u"Chèo thuyền thôi", "target": "Tiếng Việt"})
        df_train = pd.DataFrame(train_data)
        
        # Tạo test data
        test_data = []
        for a in range(len(all_tests)):
            mydict = {"feature" : all_tests[a], "target" : "Tiếng Anh"}
            test_data.append(mydict)
        df_test = pd.DataFrame(test_data)
        
        # init model naive bayes
        model = NaiveBayesModel()

        clf = model.clf.fit(df_train.feature, df_train.target)

        predicted = clf.predict(df_test["feature"]) # dự đoán cho test data # các test data được gán target là Tiếng Anh hết nên co nhãn đúng nhãn sai
        
        # Print predicted result
        print(predicted)
        
tcp = TextClassificationPredict()
tcp.get_train_data()

# Thuật toán Naive Bayes để dự đoán
